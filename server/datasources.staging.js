'use strict'

module.exports = {
  "mysql": {
    "host": "",
    "port": 3306,
    "url": process.env.JAWSDB_URL,
    "password": "",
    "name": "mysql",
    "user": "",
    "connector": "mysql"
  }
}
